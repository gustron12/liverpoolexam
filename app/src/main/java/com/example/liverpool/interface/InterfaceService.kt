package com.example.liverpool.`interface`

import com.example.liverpool.model.Model
import com.google.gson.JsonObject
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface InterfaceService {

    @GET("appclienteservices/services/v3/plp")
    fun getAll(
        @Query("force-plp") forcePLP: String = "true",
        @Query("search-string") searchString: String,
        @Query("page-number") pageNumber: String,
        @Query("number-of-items-per-page") itemPage: String
    ): Observable<JsonObject>

    @GET("appclienteservices/services/v3/plp")
    fun getAllF(
        @Query("force-plp") forcePLP: String = "true",
        @Query("search-string") searchString: String,
        @Query("page-number") pageNumber: String,
        @Query("number-of-items-per-page") itemPage: String
    ): Observable<Model.ModelProduct>

    companion object {
        fun create(): InterfaceService {

            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://shoppapp.liverpool.com.mx")
                .build()

            return retrofit.create(InterfaceService::class.java)
        }
    }
}