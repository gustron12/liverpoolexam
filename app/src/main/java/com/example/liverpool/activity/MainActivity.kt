package com.example.liverpool.activity

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.TypedValue
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.liverpool.R
import com.example.liverpool.`interface`.InterfaceService
import com.example.liverpool.adapter.AdapterRecords
import com.example.liverpool.model.ChipSearch
import com.example.liverpool.model.Model
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.realm.Realm
import io.realm.RealmResults
import io.realm.kotlin.where
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    lateinit var realm: Realm
    lateinit var adapterRecords: AdapterRecords

    private var disposable: Disposable? = null
    private var records: ArrayList<Model.Records> = ArrayList()
    lateinit var realmResults: RealmResults<ChipSearch>

    //var arrayListChips = ArrayList<ChipSearch>()
    var pageNumber: Int = 1
    var lastSearch: String = ""
    private val VISIBLE_THRESHOLD: Int = 1
    private var lastVisibleItem: Int = 0
    private var totalItemCount: Int = 0

    private var loading = false
    lateinit var linearLayoutManager: LinearLayoutManager

    private val wikiApiServe by lazy {
        InterfaceService.create()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        realm = Realm.getDefaultInstance()

        adapterRecords = object : AdapterRecords(this@MainActivity, records) {
            override fun getRecord(record: Model.Records) {
                val intent = Intent(this@MainActivity, RecordDetail::class.java)
                intent.putExtra("record", record)
                startActivity(intent)
            }
        }

        linearLayoutManager = LinearLayoutManager(this@MainActivity)

        recyclerView.apply {
            layoutManager = linearLayoutManager
            adapter = adapterRecords
            setHasFixedSize(true)
        }

        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(
                recyclerView: RecyclerView,
                dx: Int, dy: Int
            ) {
                super.onScrolled(recyclerView, dx, dy)
                totalItemCount = linearLayoutManager.itemCount
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition()

                if (!loading && totalItemCount <= lastVisibleItem + VISIBLE_THRESHOLD) {
                    pageNumber++
                    searchText(lastSearch, pageNumber)
                }
            }
        })
        val searchinResults = realm.where<ChipSearch>().findAll()
        realm.executeTransaction {
            realmResults = searchinResults

            if (realmResults.size > 0) {
                linearBusqueda.visibility = View.VISIBLE
                showChips()
            } else {
                linearBusqueda.visibility = View.GONE
            }
        }

        btnSearch.setOnClickListener {
            hideKeyboard()
            val searchText = edtSearch.editText!!.text.toString()

            if (!searchText.equals("")) {
                createChip(chipGroup, searchText, true)

                searchText(searchText, 1)
            } else {
                Toast.makeText(
                    this,
                    "Ingrese el nombre de un producto para buscar",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    fun searchText(searchingText: String, page: Int) {
        lastSearch = searchingText
        pageNumber = page
        linearBusqueda.visibility = View.VISIBLE
        loading = true
        if (pageNumber == 1) {
            recyclerView.showShimmer()
        } else {
            progressBar.visibility = View.VISIBLE
        }

        disposable = wikiApiServe.getAllF("true", searchingText, pageNumber.toString(), "15")
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->
                    recyclerView.hideShimmer()

                    var temp: ArrayList<Model.Records> = ArrayList()
                    var pos = 0
                    if (pageNumber > 1) {
                        pos = pageNumber - 1
                        temp.addAll(records)
                    }

                    temp.addAll(result.plpResults!!.records)
                    records.clear()
                    records.addAll(temp)

                    //adapterRecords.notifyItemInserted(lastVisibleItem + 1)
                    //recyclerView.adapter!!.notifyItemInserted(pageNumber * 15 + 1)
                    if (pos == 0)
                        linearLayoutManager.scrollToPosition(0)
                    else
                        linearLayoutManager.scrollToPosition(pos * 15 - 1)

                    loading = false
                    progressBar.visibility = View.GONE
                },
                { error ->
                    Toast.makeText(this, error.message, Toast.LENGTH_SHORT).show()
                }
            )
    }

    fun Activity.hideKeyboard() {
        hideKeyboard(currentFocus ?: View(this))
    }

    fun Context.hideKeyboard(view: View) {
        val inputMethodManager =
            getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }

    private fun createChip(chipGroup: ChipGroup, text: String, value: Boolean) {
        val index = realmResults.indexOfFirst { it.searchName.equals(text) }

        if (index == -1) {
            val chip = layoutInflater.inflate(R.layout.view_chip, null, false) as Chip
            val paddingDp = TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                10f,
                resources.displayMetrics
            ).toInt()
            chip.setPadding(paddingDp, paddingDp, paddingDp, paddingDp)
            chip.text = text
            chip.tag = text

            if (value) {
                chip.setOnCloseIconClickListener {
                    val subindex = realmResults.indexOfFirst { it.searchName.equals(text) }

                    realm.executeTransaction {
                        realmResults.removeAt(subindex)
                        chipGroup.removeView(chip)
                    }
                }
            } else {
                chip.isCloseIconVisible = false
                chip.isCloseIconEnabled = false
            }

            chip.setOnClickListener {
                lastSearch = text
                edtSearch.editText!!.setText(text)
                searchText(text, 1)
            }

            chipGroup.addView(chip)

            realm.executeTransaction {
                it.insertOrUpdate(ChipSearch(text))
            }
        }
    }

    fun showChips() {
        for (item in realmResults) {
            val chip = layoutInflater.inflate(R.layout.view_chip, null, false) as Chip
            val paddingDp = TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                10f,
                resources.displayMetrics
            ).toInt()
            chip.setPadding(paddingDp, paddingDp, paddingDp, paddingDp)
            chip.text = item.searchName
            chip.tag = item.searchName

            chip.setOnCloseIconClickListener {
                realm.executeTransaction {
                    item.deleteFromRealm()
                    chipGroup.removeView(chip)
                }
            }

            chip.setOnClickListener {
                lastSearch = item.searchName
                edtSearch.editText!!.setText(item.searchName)
                searchText(item.searchName, 1)
            }

            chipGroup.addView(chip)
        }
    }
}
