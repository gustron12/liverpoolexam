package com.example.liverpool.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.example.liverpool.R
import com.example.liverpool.model.Model
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_record_detail.*

class RecordDetail : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_record_detail)

        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        toolbar?.navigationIcon =
            ContextCompat.getDrawable(this, R.drawable.ic_keyboard_arrow_left_black_24dp)
        toolbar?.setNavigationOnClickListener { finish() }

        val record: Model.Records =
            intent.extras!!.getSerializable("record") as Model.Records

        Picasso.with(this@RecordDetail).load(record.lgImage).into(imgRecord)

        textViewName!!.text = record.productDisplayName
        txtPrice!!.text = "$${record.listPrice}"
        txtLocation!!.text = record.category
        txtIDRecord!!.text = "ID: ${record.productId}"
    }
}