package com.example.liverpool.util

import androidx.multidex.MultiDexApplication
import io.realm.Realm
import io.realm.RealmConfiguration
import io.realm.rx.RealmObservableFactory

class Application: MultiDexApplication() {
    override fun onCreate() {
        super.onCreate()

        Realm.init(this)

        val config = RealmConfiguration.Builder().name("liverpoolExam.realm").schemaVersion(1)
            .deleteRealmIfMigrationNeeded().rxFactory(RealmObservableFactory(false)).build()
        Realm.setDefaultConfiguration(config)
    }
}