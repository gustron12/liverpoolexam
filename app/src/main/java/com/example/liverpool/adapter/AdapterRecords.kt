package com.example.liverpool.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.liverpool.R
import com.example.liverpool.model.Model
import com.google.android.material.card.MaterialCardView
import com.squareup.picasso.Picasso

abstract class AdapterRecords(
    val context: Context,
    var arrayList: ArrayList<Model.Records>
) :
    RecyclerView.Adapter<AdapterRecords.AdapterRecordsViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterRecordsViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return AdapterRecordsViewHolder(inflater, parent)
    }

    override fun getItemCount(): Int = arrayList.size

    override fun onBindViewHolder(holder: AdapterRecordsViewHolder, position: Int) {
        val card: Model.Records = arrayList[position]
        holder.bind(card)
    }

    inner class AdapterRecordsViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(inflater.inflate(R.layout.row_record, parent, false)) {

        private var imgRecord: ImageView? = null
        private var textViewName: TextView? = null
        private var txtPrice: TextView? = null
        private var txtLocation: TextView? = null
        private var txtIDRecord: TextView? = null
        private var cardView: MaterialCardView? = null
        private var linearLayout: LinearLayout? = null

        init {
            imgRecord = itemView.findViewById(R.id.imgRecord)
            txtPrice = itemView.findViewById(R.id.txtPrice)
            txtLocation = itemView.findViewById(R.id.txtLocation)
            txtIDRecord = itemView.findViewById(R.id.txtIDRecord)
            textViewName = itemView.findViewById(R.id.textViewName)
            cardView = itemView.findViewById(R.id.cardView)
            linearLayout = itemView.findViewById(R.id.linearLayout)
        }

        fun bind(record: Model.Records) {
            linearLayout!!.visibility = View.VISIBLE

            Picasso.with(context).load(record.lgImage).into(imgRecord)

            textViewName!!.text = record.productDisplayName
            txtPrice!!.text = "$${record.listPrice}"
            txtLocation!!.text = record.category
            txtIDRecord!!.text = record.productId

            cardView!!.setOnClickListener {
                getRecord(record)
            }
        }
    }

    fun refreshData(arrayListData: ArrayList<Model.Records>) {
        arrayList = arrayListData
        notifyDataSetChanged()
    }

    fun refreshDataAdd(arrayListData: ArrayList<Model.Records>) {
        arrayList = arrayListData
        notifyItemRangeInserted(arrayList.size - 15, arrayList.size)
    }

    open fun setItems() {
        notifyDataSetChanged()
    }

    abstract fun getRecord(record: Model.Records)
}