package com.example.liverpool.model

import io.realm.RealmObject
import io.realm.annotations.RealmClass
import java.io.Serializable

object Model {
    class ModelProduct : Serializable {
        var pageType: String = ""
        var plpResults: PlpResults? = null
        var status: Status? = null

        override fun toString(): String {
            return "ClassPojo [pageType = $pageType, plpResults = $plpResults, status = $status]"
        }
    }

    class PlpResults : Serializable {
        var refinementGroups: ArrayList<RefinementGroups> = ArrayList()
        var navigation: Navigation? = null
        var sortOptions: ArrayList<SortOptions> = ArrayList()
        var records: ArrayList<Records> = ArrayList()
        var plpState: PlpState? = null
        var label: String = ""

        override fun toString(): String {
            return "ClassPojo [refinementGroups = $refinementGroups, navigation = $navigation, sortOptions = $sortOptions, records = $records, plpState = $plpState, label = $label]"
        }
    }

    class Navigation : Serializable {
        var current: ArrayList<Current> = ArrayList()
        var ancester: ArrayList<String> = ArrayList()
        var childs: ArrayList<String> = ArrayList()

        override fun toString(): String {
            return "ClassPojo [current = $current, ancester = $ancester, childs = $childs]"
        }
    }

    class Current : Serializable {
        var label: String = ""
        var categoryId: String = ""

        override fun toString(): String {
            return "ClassPojo [label = $label, categoryId = $categoryId]"
        }
    }

    class Records : Serializable {
        var isMarketPlace: String = ""
        var seller: String = ""
        var maximumListPrice: String = ""
        var groupType: String = ""
        var smImage: String = ""
        var maximumPromoPrice: String = ""
        var isImportationProduct: String = ""
        var brand: String = ""
        var productType: String = ""
        var marketplaceSLMessage: String = ""
        var plpFlags: ArrayList<String> = ArrayList()
        var productId: String = ""
        var minimumListPrice: String = ""
        var xlImage: String = ""
        var skuRepositoryId: String = ""
        var productAvgRating: String = ""
        var marketplaceBTMessage: String = ""
        var promoPrice: String = ""
        var minimumPromoPrice: String = ""
        var productDisplayName: String = ""
        var productRatingCount: String = ""
        var isHybrid: String = ""
        var variantsColor: ArrayList<VariantsColor> = ArrayList()
        var lgImage: String = ""
        var category: String = ""
        var listPrice: String = ""

        override fun toString(): String {
            return "ClassPojo [isMarketPlace = $isMarketPlace, seller = $seller, maximumListPrice = $maximumListPrice, groupType = $groupType, smImage = $smImage, maximumPromoPrice = $maximumPromoPrice, isImportationProduct = $isImportationProduct, brand = $brand, productType = $productType, marketplaceSLMessage = $marketplaceSLMessage, plpFlags = $plpFlags, productId = $productId, minimumListPrice = $minimumListPrice, xlImage = $xlImage, skuRepositoryId = $skuRepositoryId, productAvgRating = $productAvgRating, marketplaceBTMessage = $marketplaceBTMessage, promoPrice = $promoPrice, minimumPromoPrice = $minimumPromoPrice, productDisplayName = $productDisplayName, productRatingCount = $productRatingCount, isHybrid = $isHybrid, variantsColor = $variantsColor, lgImage = $lgImage, category = $category, listPrice = $listPrice]"
        }
    }

    class VariantsColor : Serializable {
        var colorName: String = ""
        var colorImageURL: String = ""
        var colorHex: String = ""

        override fun toString(): String {
            return "ClassPojo [colorName = $colorName, colorImageURL = $colorImageURL, colorHex = $colorHex]"
        }
    }

    class RefinementGroups : Serializable {
        var name: String = ""
        var refinement: ArrayList<Refinement> = ArrayList()
        var dimensionName: String = ""
        var multiSelect: String = ""

        override fun toString(): String {
            return "ClassPojo [name = $name, refinement = $refinement, dimensionName = $dimensionName, multiSelect = $multiSelect]"
        }
    }

    class Refinement : Serializable {
        var refinementId: String = ""
        var count: String = ""
        var label: String = ""
        var selected: String = ""

        override fun toString(): String {
            return "ClassPojo [refinementId = $refinementId, count = $count, label = $label, selected = $selected]"
        }
    }

    class SortOptions : Serializable {
        var sortBy: String = ""
        var label: String = ""

        override fun toString(): String {
            return "ClassPojo [sortBy = $sortBy, label = $label]"
        }
    }

    class PlpState : Serializable {
        var lastRecNum: String = ""
        var totalNumRecs: String = ""
        var firstRecNum: String = ""
        var currentSortOption: String = ""
        var categoryId: String = ""
        var recsPerPage: String = ""
        var currentFilters: String = ""

        override fun toString(): String {
            return "ClassPojo [lastRecNum = $lastRecNum, totalNumRecs = $totalNumRecs, firstRecNum = $firstRecNum, currentSortOption = $currentSortOption, categoryId = $categoryId, recsPerPage = $recsPerPage, currentFilters = $currentFilters]"
        }
    }

    class Status : Serializable {
        var status: String = ""
        var statusCode: String = ""

        override fun toString(): String {
            return "ClassPojo [status = $status, statusCode = $statusCode]"
        }
    }
}