package com.example.liverpool.model

import io.realm.RealmObject
import io.realm.annotations.RealmClass
import java.io.Serializable

@RealmClass
open class ChipSearch (
    public open var searchName: String = ""
): RealmObject(), Serializable