package com.example.liverpool.model

import java.io.Serializable

class Product(
    var name: String,
    var photo: String,
    var realName: String,
    var height: String,
    var power: String,
    var abilities: String,
    var groups: String
) : Serializable